#!/bin/bash
# Установка Российских корневых сертификатов в Debian

# Проверяем запущен ли скрипт из под суперпользователя root
if [[ ! $(whoami | grep -a root) ]]; then
    echo "Внимание! Скрипт должен быть запущен из под суперпользователя root!"
    exit 1
fi

# Вывод заголовка
echo "Установка российских корневых сертификатов в Debian"
echo "==================================================="

# Скачиваем сертификаты 
wget -P /root/ https://gu-st.ru/content/lending/russian_trusted_root_ca_pem.crt
wget -P /root/ https://gu-st.ru/content/lending/russian_trusted_sub_ca_pem.crt

# Создаём каталог под сертификаты
if ! [[-d "/usr/local/share/ca-certificates" ]]; then
	mkdir /usr/local/share/ca-certificates
fi

# Перемещаем сертификаты
mv /root/russian_trusted_root_ca_pem.crt /usr/local/share/ca-certificates
mv /root/russian_trusted_sub_ca_pem.crt /usr/local/share/ca-certificates

# Устанавливаем сертификаты
update-ca-certificates -v

# Проверка сертификатов
trust list | grep -A 2 -B 2 "Russian"
